import { gql } from 'apollo-server'
import { createTestClient } from 'apollo-server-testing'
import server from '../src/server'

const PING = gql`
  query ping {
    ping {
      message
    }
  }
`

const UNKNOWN_PROP_PING = gql`
  query ping {
    ping {
      failingProperty
    }
  }
`

const MISSING_PROP_PING = gql`
  query ping {
    ping
  }
`

const { query } = createTestClient(server as any)

describe('server ping test', () => {
  it('should respond with a running message and a date-time', async () => {
    const ping = await query({ query: PING })
    const message = ping?.data?.ping?.message?.replace(/\.\d{3}Z/, '')
    const expected = 'Apollo server is running 🚀\n' + new Date().toISOString().replace(/\.\d{3}Z/, '')
    expect(message).toBe(expected)
  })

  it('should respond with an error for a unknown property', async () => {
    const ping = await query({ query: UNKNOWN_PROP_PING })
    const errorMessage = ping?.errors?.[0]?.message
    expect(errorMessage).toBe('Cannot query field "failingProperty" on type "Ping".')
  })

  it('should respond with an error for a missing property', async () => {
    const ping = await query({ query: MISSING_PROP_PING })
    const errorMessage = ping?.errors?.[0]?.message
    expect(errorMessage).toBe('Field "ping" of type "Ping" must have a selection of subfields. Did you mean "ping { ... }"?')
  })
})
