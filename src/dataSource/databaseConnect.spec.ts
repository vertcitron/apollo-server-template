import { MongoMemoryServer } from 'mongodb-memory-server'
import { prop, getModelForClass } from '@typegoose/typegoose'
import { databaseConnect, databaseDisconnect, buildConnectionString } from './databaseConnect'

// May require additional time for downloading MongoDB binaries
jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000

class Simple {
  @prop()
  public name!: string
}

describe('database connexion tests', () => {
  it('should connect, write and read a single model document', async () => {
    const mongoServer = new MongoMemoryServer()
    const mongoUri = await mongoServer.getUri()
    await databaseConnect(mongoUri)
    const SimpleModel = getModelForClass(Simple)
    await SimpleModel.create({ name: 'test' })
    const testBack = await SimpleModel.findOne()
    expect(testBack?.name).toBe('test')
    await databaseDisconnect()
    await mongoServer.stop()
  })

  it('should exit with an error if not connected', async () => {
    async function tryConnect (): Promise<any> {
      return await databaseConnect('mongo+srv://test-user:test-pwd@mongo-host/database')
    }
    await expect(tryConnect()).rejects.toBeTruthy()
  })
})

describe('connexion URL test', () => {
  it('should build a connexion string as initialized', () => {
    process.env.DB_PROTOCOL = 'mongo+srv'
    process.env.DB_USER = 'test-user'
    process.env.DB_PASSWORD = 'test-pwd'
    process.env.DB_HOST = 'mongo-host'
    process.env.DB_DATABASE = 'database'
    process.env.DB_QUERYSTRING = '?false=true'
    expect(buildConnectionString()).toBe('mongo+srv://test-user:test-pwd@mongo-host/database?false=true')
  })

  it('should make an empty connection string without env variables', () => {
    delete process.env.DB_PROTOCOL
    delete process.env.DB_USER
    delete process.env.DB_PASSWORD
    delete process.env.DB_HOST
    delete process.env.DB_DATABASE
    delete process.env.DB_QUERYSTRING
    expect(buildConnectionString()).toBe('://:@/')
  })
})
