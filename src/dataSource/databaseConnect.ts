import mongoose from 'mongoose'

export function buildConnectionString (): string {
  const protocol = process.env.DB_PROTOCOL ?? ''
  const user = process.env.DB_USER ?? ''
  const password = process.env.DB_PASSWORD ?? ''
  const host = process.env.DB_HOST ?? ''
  const database = process.env.DB_DATABASE ?? ''
  const queryString = process.env.DB_QUERYSTRING ?? ''
  return `${protocol}://${user}:${password}@${host}/${database}${queryString}`
}

export async function databaseConnect (connectionString: string): Promise<void> {
  const connect = async (): Promise<any> => {
    return await mongoose
      .connect(connectionString, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      })
  }
  return await connect()
    .then(() => {
      mongoose.connection.on('disconnected', () => {
        connect()
      })
    })
}

export async function databaseDisconnect (): Promise<void> {
  return await mongoose.disconnect()
}
