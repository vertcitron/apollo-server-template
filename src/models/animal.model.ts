import { prop, getModelForClass } from '@typegoose/typegoose'

export class Translation {
  @prop()
  public enUS!: string

  @prop()
  public frFR?: string
}

export class Species {
  @prop({ type: () => Translation })
  public name!: Translation

  @prop({ type: [Number] })
  public ages?: number[]
}

export class Animal {
  @prop({ type: () => Translation })
  public name!: Translation

  @prop({ type: () => Species })
  public generic!: Species

  @prop({ type: () => [Species] })
  public species?: Species[]
}

export default getModelForClass(Animal)
