/**
 * This is a way to modularize the server. All modules have juste to extend the Query and Mutations types
 */

import { gql } from 'apollo-server'
import ping from './ping'
import animals from './animals'

const root = gql`
  type Query {
    root: String!
  }
  type Mutation {
    root: String!
  }
`

export default [
  root,
  ping,
  animals
]
