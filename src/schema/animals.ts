import { gql } from 'apollo-server'

export default gql`
  type Translation {
    enUS: String!
    frFR: String!
  }
  
  type Species {
    name: Translation!
    ages: [Int]
  }
  
  type Animal {
    name: Translation!
    generic: Species!
    species: [Species]
  }
  
  extend type Query {
    getAnimals: [Animal]
  }
`
