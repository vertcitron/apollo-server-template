import { gql } from 'apollo-server'

export default gql`
  type Ping {
    message: String!
  }
    
  extend type Query {
    ping: Ping
  }
`
