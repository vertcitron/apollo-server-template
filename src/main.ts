/* eslint-disable import/first */
import * as dotenv from 'dotenv'
dotenv.config()

import server from './server'
import { databaseConnect, buildConnectionString } from './dataSource/databaseConnect'

databaseConnect(buildConnectionString())
  .then(() => {
    server
      .listen()
      .then(({ url }): void => console.log(`🚀 Server on air at ${url}.`))
  })
  .catch((error) => {
    console.error(error)
  })
