import Ping from './ping'

describe('Ping resolver tests', () => {
  it('should have a Query member', () => {
    expect(Ping.Query).toBeDefined()
  })

  it('should have a Ping resolver returning the current datetime', () => {
    const message = Ping.Query.ping()?.message?.replace(/\.\d{3}Z/, '')
    const expected = 'Apollo server is running 🚀\n' + new Date().toISOString().replace(/\.\d{3}Z/, '')
    expect(message).toBe(expected)
  })
})
