import AnimalModel from '../models/animal.model'

export default {
  Query: {
    getAnimals: () => {
      return AnimalModel.find({})
    }
  }
}
