import ping from './ping'
import animals from './animals'

const Query = {
  ...ping.Query,
  ...animals.Query
}

const Mutation = {
  ...ping.Mutation
}

export default { Query, Mutation }
