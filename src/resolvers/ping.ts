export default {
  Query: {
    ping: () => ({
      message: 'Apollo server is running 🚀\n' + new Date().toISOString()
    })
  },
  Mutation: {}
}
