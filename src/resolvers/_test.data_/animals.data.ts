export default {
  name: { enUS: 'cat', frFR: 'chat' },
  generic: {
    name: { enUS: 'generic', frFR: 'générique' },
    ages: [0, 6, 12, 18, 24, 30, 36, 48, 54, 60, 66, 72, 78, 84, 90, 96, 102]
  },
  species: [
    {
      name: { enUS: 'bombay', frFR: 'bombay' },
      ages: []
    }
  ]
}
