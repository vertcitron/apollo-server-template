import Animal from './animals'
import AnimalModel from '../models/animal.model'
import { MongoMemoryServer } from 'mongodb-memory-server'
import { databaseConnect, databaseDisconnect } from '../dataSource/databaseConnect'
import cat from './_test.data_/animals.data'

// May require additional time for downloading MongoDB binaries
jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000

describe('animals resolver', () => {
  const mongoServer = new MongoMemoryServer()

  beforeAll(async () => {
    const mongoUri = await mongoServer.getUri()
    await databaseConnect(mongoUri)
    await AnimalModel.create(cat)
  })

  afterAll(async () => {
    await databaseDisconnect()
    await mongoServer.stop()
  })

  it('should return a cat on getAnimals Query', async () => {
    const received = await Animal.Query.getAnimals()
    expect(received.length).toBe(1)
    expect(received[0].name).toEqual(expect.objectContaining(cat.name))
  })
})
